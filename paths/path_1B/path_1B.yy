{
    "id": "a1994eec-7a53-4415-ab0b-18e24c2d099c",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_1B",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "bbd4f04b-7250-40d9-adda-dcd4962a4dd2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64,
            "speed": 100
        },
        {
            "id": "43e53bb2-c98e-411e-82a1-5d211556ebb5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 64,
            "speed": 100
        },
        {
            "id": "381ea1e5-71ef-46f4-b53f-9490687332f4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 64,
            "speed": 100
        },
        {
            "id": "64e1cdab-6871-46de-9792-91e36b3d350d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 64,
            "speed": 100
        },
        {
            "id": "e478b824-46a0-48fd-8fb1-d1bd42bb7056",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 160,
            "speed": 100
        },
        {
            "id": "e58f7a64-49a5-48b8-a7a7-548d7bcc03a5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 160,
            "speed": 100
        },
        {
            "id": "5e37e579-f578-4ecb-9d77-2ca3973243a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 352,
            "speed": 100
        },
        {
            "id": "fbbd333e-88fc-46e8-9f94-ebecd9597bdc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 352,
            "speed": 100
        },
        {
            "id": "23964858-8296-4a08-87d7-9594fcbc4d10",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 352,
            "speed": 100
        },
        {
            "id": "fbf233e0-84a6-426f-bb9b-246751fcae03",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 448,
            "speed": 100
        },
        {
            "id": "82ad5bb1-875c-4ec2-a375-cf13cb9c0ee7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 448,
            "speed": 100
        },
        {
            "id": "67f59951-50a9-42cb-bd0b-e99b086d8ea4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 704,
            "speed": 100
        },
        {
            "id": "efe77673-240e-4396-bdc9-e8baefe06b74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "0598cf0d-74bf-46db-873a-b020dc32e92e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 544,
            "speed": 100
        },
        {
            "id": "3b9584b8-e2f1-47be-a45b-97da2785602c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "383565a2-261d-4a5d-9cf7-47972700a3e9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 704,
            "speed": 100
        },
        {
            "id": "e2db2d2c-2ba4-49fd-a2ba-3067f62e6ee3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 256,
            "speed": 100
        },
        {
            "id": "02c0ae91-29ed-4ecc-a940-dc583e2dc7ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 256,
            "speed": 100
        },
        {
            "id": "84eb5a2e-3a72-4338-a756-c1018135a7da",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 256,
            "speed": 100
        },
        {
            "id": "927b4f20-ffd6-4a2a-b08f-785f0a81e700",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 192,
            "speed": 100
        },
        {
            "id": "972b33b2-105b-432a-ab38-57819f34ae19",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 256,
            "speed": 100
        },
        {
            "id": "0fb2c628-36b6-49f9-b24d-5c1361ddda82",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 256,
            "speed": 100
        },
        {
            "id": "376479a1-f5bf-42a7-ba31-c040ef9204a7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 352,
            "speed": 100
        },
        {
            "id": "be3459e2-c166-42f7-8c66-dde0dff35082",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 352,
            "speed": 100
        },
        {
            "id": "76633efe-2ae0-42e7-8e0a-59992a4a6c65",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 704,
            "speed": 100
        },
        {
            "id": "77ba712e-126b-4271-822f-5daa42440344",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 704,
            "speed": 100
        },
        {
            "id": "47f305d5-7d9b-4620-9ca6-48cd7ed983f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 704,
            "speed": 100
        },
        {
            "id": "a7494477-6f7f-4271-80a8-3cdc2419182c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 544,
            "speed": 100
        },
        {
            "id": "78caac8a-7a28-4839-8a56-a12f70eed814",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 640,
            "speed": 100
        },
        {
            "id": "c4e9c59b-3252-4eaa-937b-bbc4ae2d890f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 640,
            "speed": 100
        },
        {
            "id": "4c4162bf-975c-4d76-b6cd-60b213c0bb08",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 704,
            "speed": 100
        },
        {
            "id": "c76dadd4-e134-4f3b-b9d6-1b35547d2921",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 704,
            "speed": 100
        },
        {
            "id": "6d7b684d-ec2f-44e8-b803-5cdc961d522f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 544,
            "speed": 100
        },
        {
            "id": "0c92ea0c-a37d-4f97-8071-d160861f30e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 544,
            "speed": 100
        },
        {
            "id": "17bb6dbe-d383-4e4c-ba0f-98f3f0ce854c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 416,
            "speed": 100
        },
        {
            "id": "95abe7e2-01d6-460a-9f16-93fe14635482",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 416,
            "speed": 100
        },
        {
            "id": "0187b0e1-69fc-4959-9f46-9491929f7c06",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 448,
            "speed": 100
        },
        {
            "id": "3bf8ac2c-1a17-4c3c-a080-7162b369eaa3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 512,
            "speed": 100
        },
        {
            "id": "66c81a5a-ebc1-4b16-8af7-3addfbc25475",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 512,
            "speed": 100
        },
        {
            "id": "3fec58e3-7f80-473e-afa7-75fd73d3ee99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 512,
            "speed": 100
        },
        {
            "id": "9aca5c61-5728-49c4-9dfa-dc355ea7d908",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 512,
            "speed": 100
        },
        {
            "id": "b3e4a06d-4e90-4596-82d6-24af55dca89c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 608,
            "speed": 100
        },
        {
            "id": "31b81d76-4801-46cb-9d53-e3233c5c5189",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 608,
            "speed": 100
        },
        {
            "id": "3386ffb4-983a-4025-87e3-b6889334602d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 608,
            "speed": 100
        },
        {
            "id": "788396d2-be6c-4e64-a056-3ebae4a67ff4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 608,
            "speed": 100
        },
        {
            "id": "2690fadf-28fd-482a-9632-a29e4f1bd36c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 512,
            "speed": 100
        },
        {
            "id": "bc7e8a79-c6e6-43cc-bc16-079a236b35c6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 512,
            "speed": 100
        },
        {
            "id": "ee702a75-8f1b-4ba8-b2ea-3032e5376604",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 224,
            "speed": 100
        },
        {
            "id": "92e06cb1-4f21-49a6-83d2-1d9511659ab7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 224,
            "speed": 100
        },
        {
            "id": "2a18f32d-1dfa-4138-a614-6641d7819473",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 224,
            "speed": 100
        },
        {
            "id": "e0a709ce-ca0c-4558-92fa-0739e8dc5b31",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 416,
            "speed": 100
        },
        {
            "id": "5cf6fbf1-2146-4299-802c-63304a69a136",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 416,
            "speed": 100
        },
        {
            "id": "864ed234-28db-4c4f-83c4-f30612c0b67e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 544,
            "speed": 100
        },
        {
            "id": "0af529ac-19ef-469b-909e-4cd83d143b25",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 544,
            "speed": 100
        },
        {
            "id": "a41cb154-34d1-4d95-9849-405a42840241",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 448,
            "speed": 100
        },
        {
            "id": "00be4710-486b-4157-8de1-e4b9f11ef231",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 448,
            "speed": 100
        },
        {
            "id": "259eed4b-d81b-4574-b3fa-0a121c9d5831",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 320,
            "speed": 100
        },
        {
            "id": "00a8062e-9092-4d05-9f6e-05104f74df37",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 160,
            "speed": 100
        },
        {
            "id": "790ee162-b3e8-451f-be5b-4d48037e58bc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 160,
            "speed": 100
        },
        {
            "id": "611c5ff8-34b2-4b02-a2af-462c4b338f8e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 64,
            "speed": 100
        },
        {
            "id": "b424adbd-6705-451c-aeeb-adfee1481c8d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 64,
            "speed": 100
        },
        {
            "id": "f538998e-0263-4c91-8379-3307fd108cf6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 128,
            "speed": 100
        },
        {
            "id": "c1dc9caa-f440-47a9-b56c-d2939d82eab5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 128,
            "speed": 100
        },
        {
            "id": "a2e29751-6217-448d-b523-94eaf64dd1f3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 64,
            "speed": 100
        },
        {
            "id": "006e9d5d-2ba7-46fa-bdbd-587979f2df18",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 128,
            "speed": 100
        },
        {
            "id": "9421dec8-c080-4f2e-8b12-fe3472ae127d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 128,
            "speed": 100
        },
        {
            "id": "f8f47450-476a-4459-b475-848c9b406932",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 64,
            "speed": 100
        },
        {
            "id": "04c918f4-8d8d-4d61-809b-ac3c7760cd23",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 128,
            "speed": 100
        },
        {
            "id": "5acb567c-edd1-4bd9-a42c-1f9698ecfbce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 128,
            "speed": 100
        },
        {
            "id": "36780387-3544-4f8e-a460-93fa50ec8bbf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 64,
            "speed": 100
        },
        {
            "id": "1cb61c07-113f-4670-81e4-b14401ed5f76",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 128,
            "speed": 100
        },
        {
            "id": "5a3d987c-8978-4722-a8e1-25422dcf7c41",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 128,
            "speed": 100
        },
        {
            "id": "cd9cf604-35d4-4100-8a66-ef33eee29f72",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 128,
            "speed": 100
        },
        {
            "id": "fa352c09-e98c-4e4a-9766-8cb8fc1e224d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "e19ac708-0dae-4b11-bc2b-a84cf82288b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 320,
            "speed": 100
        },
        {
            "id": "0cd3e018-a247-4f5d-9aa6-77c59edf1a16",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 320,
            "speed": 100
        },
        {
            "id": "d21ac24a-8c56-4d0c-bb0c-354228b8dac4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 416,
            "speed": 100
        },
        {
            "id": "d667db98-3595-47dc-b427-f211dc766633",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "6862d304-acbf-4e9d-ba3a-9704e44ef412",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 480,
            "speed": 100
        },
        {
            "id": "7b33e85e-eb1f-4536-95d1-3a0bddf333d9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "d1139033-970d-4901-a6cb-48a83e3c12ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "6614806d-2317-4017-a2f2-6b0846d51e69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 416,
            "speed": 100
        },
        {
            "id": "67689fe6-c5b1-42d8-a152-3c623e99a3d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 320,
            "speed": 100
        },
        {
            "id": "66056d35-407d-4d92-900d-5e08cdf2a7c1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 320,
            "speed": 100
        },
        {
            "id": "465f9789-e249-4bde-b430-71552d474df4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 128,
            "speed": 100
        },
        {
            "id": "9f64ddd6-29ad-4e53-b8bf-76b6088fb881",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 128,
            "speed": 100
        },
        {
            "id": "282ecbb9-0ebc-48dd-b75f-69c4578e3c82",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 64,
            "speed": 100
        },
        {
            "id": "eccc9d37-0091-4ea8-aae2-4e719edbce11",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 64,
            "speed": 100
        },
        {
            "id": "0012df6e-9c99-4d47-8586-f22ed6a92418",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 160,
            "speed": 100
        },
        {
            "id": "2e4e7ec9-30c2-43b3-bb97-8944d4fd6c25",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 160,
            "speed": 100
        },
        {
            "id": "1c74251d-9b33-4b02-9551-b9692b3b8de9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 320,
            "speed": 100
        },
        {
            "id": "27b14c24-434d-469a-a652-281cac28d5cf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 320,
            "speed": 100
        },
        {
            "id": "60985c42-4f33-4e40-901a-2701c0a12e6d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 224,
            "speed": 100
        },
        {
            "id": "67ffea6f-1e69-4118-9ceb-497ab8c33bce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 224,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}