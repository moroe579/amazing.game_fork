{
    "id": "09c484c8-8bc0-4543-a431-54bbdfaf3842",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_1A",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "f9de88c4-da08-4a3d-8ef2-1ec3c34098cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64,
            "speed": 100
        },
        {
            "id": "a068a65a-fe6e-4892-b897-2f12b32899f8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 416,
            "speed": 100
        },
        {
            "id": "6572843d-ad04-476b-922c-5f43e509e7b9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 416,
            "speed": 100
        },
        {
            "id": "0ca39489-d6fc-4c2f-957c-45167d53be43",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 416,
            "speed": 100
        },
        {
            "id": "9d68eb79-6c1d-4c72-bfca-bc19e87bc808",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 608,
            "speed": 100
        },
        {
            "id": "2d475995-2bf8-43fa-a3a3-01bca3fd1ac3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "294f902d-4b37-4f61-8937-784ce3395f49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 608,
            "speed": 100
        },
        {
            "id": "5b8cf462-7ccc-4566-85c2-724069a355d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 608,
            "speed": 100
        },
        {
            "id": "23e37b45-649c-4418-b484-0a453ed85293",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 512,
            "speed": 100
        },
        {
            "id": "f4082499-80de-42ae-97ca-c3a57faa32d4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 704,
            "speed": 100
        },
        {
            "id": "71afe02f-51ee-45c6-a01b-2c107243143b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 704,
            "speed": 100
        },
        {
            "id": "49024974-5da4-4f7f-9d9d-e7942d901af5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 704,
            "speed": 100
        },
        {
            "id": "7b7d8fad-41b8-45c8-8b2a-6bd730e5ac8c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 480,
            "speed": 100
        },
        {
            "id": "eded1770-ae58-4ff1-8fa8-532ed2567edd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 704,
            "speed": 100
        },
        {
            "id": "1e503aa4-f6de-4ee5-9680-445371e8018d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 704,
            "speed": 100
        },
        {
            "id": "0e4db5d4-48ed-48cf-a4f5-02640f31241e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 608,
            "speed": 100
        },
        {
            "id": "f3c92f7d-96f1-4bd0-a254-6167c2a337a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 608,
            "speed": 100
        },
        {
            "id": "a309728d-ce4c-4421-881a-f1356fecad80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 480,
            "speed": 100
        },
        {
            "id": "ea1d9f68-e47c-4dca-8619-c41b2efbe01c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 480,
            "speed": 100
        },
        {
            "id": "2f18cb1b-2b3d-4007-ae7a-cc27509b0ea5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 320,
            "speed": 100
        },
        {
            "id": "389f25f8-4a24-4009-9c60-125e8e62964c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 320,
            "speed": 100
        },
        {
            "id": "eeeea927-1365-49f2-8a5c-662cb4087bb3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 320,
            "speed": 100
        },
        {
            "id": "3180a2aa-cb2a-4b5f-8124-4a1e6a802639",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 320,
            "speed": 100
        },
        {
            "id": "06d1f2b5-258d-4c9d-bea6-f6e6b29c1035",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 320,
            "speed": 100
        },
        {
            "id": "d8ce967e-2272-48e7-b876-732b82830fd0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 480,
            "speed": 100
        },
        {
            "id": "8f4db889-473d-487f-b714-e606d7a681f8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 480,
            "speed": 100
        },
        {
            "id": "5faaad43-82cc-42ce-ab39-5a36a025397b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 480,
            "speed": 100
        },
        {
            "id": "5dc26d39-0fe3-46eb-acf2-140956885fa7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 608,
            "speed": 100
        },
        {
            "id": "eb3b18d5-48b9-44b5-b791-6e6420e3cb26",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 608,
            "speed": 100
        },
        {
            "id": "fb88360d-a43a-4e49-8162-a85b82b1b5f7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 256,
            "speed": 100
        },
        {
            "id": "08101298-a445-4bea-b8b5-2ec4ad3e33b1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 256,
            "speed": 100
        },
        {
            "id": "1d1e7dfb-3c7c-459a-8a40-073345620267",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 416,
            "speed": 100
        },
        {
            "id": "c37119bf-63fb-485b-b421-521e2a17e4e9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 224,
            "speed": 100
        },
        {
            "id": "d968701f-9653-48de-a83a-08f820fdb211",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 224,
            "speed": 100
        },
        {
            "id": "03a5c894-7c13-459c-a6ce-0f673c5872b5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 64,
            "speed": 100
        },
        {
            "id": "e6620037-fea5-4ef1-adc2-6194d2ddacdc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 64,
            "speed": 100
        },
        {
            "id": "100e72c8-6cb4-44ca-b27b-9683676fd3bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 128,
            "speed": 100
        },
        {
            "id": "b3bec1a5-04d8-4d11-ade5-169e369842bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 64,
            "speed": 100
        },
        {
            "id": "85c91497-9060-4f70-be51-fceb347a9413",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 64,
            "speed": 100
        },
        {
            "id": "5910a57d-0dba-45c9-b432-faa0e68d876d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 64,
            "speed": 100
        },
        {
            "id": "196b7345-a1d0-452e-84e1-e66d48490684",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 224,
            "speed": 100
        },
        {
            "id": "9708e64a-c202-44e5-8658-32f7224d41dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 224,
            "speed": 100
        },
        {
            "id": "fbabd3be-b869-4d65-af55-6dfb2da2a5a4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "37b7ac94-ecdd-4ce0-931b-7d3767637747",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "ea2ac8b5-9538-4eaa-a3ed-5478f2f3c3dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "a0a359ec-3698-4326-af9d-9e2447a3c734",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "97612c48-c682-4789-887c-ab477f7cf943",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 320,
            "speed": 100
        },
        {
            "id": "9f867f72-3b19-4eb7-83e7-1807cc911dd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 320,
            "speed": 100
        },
        {
            "id": "93f05014-eca8-48c3-8639-c5859af31dbc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 384,
            "speed": 100
        },
        {
            "id": "55ea4152-9a2a-4433-a733-bad1505b9f3f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 384,
            "speed": 100
        },
        {
            "id": "4ecbc337-3ed8-4646-a825-2c0b07178acf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 384,
            "speed": 100
        },
        {
            "id": "4755f4e7-0b44-4aa9-bf62-9b300f4da126",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "73271331-628f-4dab-b13a-15f0a9108b9f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 384,
            "speed": 100
        },
        {
            "id": "9c385b21-5267-466f-adb9-39120c6ecad7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 256,
            "speed": 100
        },
        {
            "id": "156aede1-8353-4e0b-ae38-dafba3b05021",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 384,
            "speed": 100
        },
        {
            "id": "e9ceea51-3929-4cbc-9121-48c670183a40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "c19fa292-edd6-4191-b126-c6c5ee18bda7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "08353873-abeb-4316-aeb8-eb0139194a9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 576,
            "speed": 100
        },
        {
            "id": "ce77f445-9eaf-4891-95c5-8abed79ecd55",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 576,
            "speed": 100
        },
        {
            "id": "a31dbe2f-a53b-4129-bfb2-7fb96d204a39",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 576,
            "speed": 100
        },
        {
            "id": "ec3574c1-0b9c-44e1-8d34-d5c782817336",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 704,
            "speed": 100
        },
        {
            "id": "62ed38fd-3f8a-45e8-8543-38441ee244f6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 704,
            "speed": 100
        },
        {
            "id": "3aabe2b2-9f15-4dee-afdf-fe10e4d0a0bc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 704,
            "speed": 100
        },
        {
            "id": "59f2b725-dc4b-48fb-99b1-84edb0237f82",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 672,
            "speed": 100
        },
        {
            "id": "b8977d90-dfcc-4150-bf2d-38728fa35ae8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 672,
            "speed": 100
        },
        {
            "id": "93a454e1-85cc-41d2-97dc-ed850fcf7446",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 576,
            "speed": 100
        },
        {
            "id": "af2c3a3b-9c42-4e05-a548-778775a6f3d7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 576,
            "speed": 100
        },
        {
            "id": "218c9a5f-2805-4d5c-9566-46defe4f64ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "0853474a-2a17-4775-9ac6-a36bb5368e24",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 384,
            "speed": 100
        },
        {
            "id": "bd0e6329-38f4-4487-b06a-f9cb2f3b2c54",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 224,
            "speed": 100
        },
        {
            "id": "4d9675be-29d4-4fa6-b7cf-ea236d510831",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 224,
            "speed": 100
        },
        {
            "id": "1a7c9c6a-5a9b-4e5e-86e7-8b15ae4b5fb9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 288,
            "speed": 100
        },
        {
            "id": "d5b60b98-fdef-4fcd-8454-4ff6c9953885",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 224,
            "speed": 100
        },
        {
            "id": "e3a0a21a-d6eb-4a41-818d-9ed48cae947f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 160,
            "speed": 100
        },
        {
            "id": "764bc06e-a263-4763-b9f6-6e07a7800be5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 160,
            "speed": 100
        },
        {
            "id": "ff263016-0916-48a2-ba1e-cbb6f4d996ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "4c22e473-0953-4227-ad04-9a391f187bbf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "6361673e-391b-4fad-93a4-ab7be0f8ef13",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 64,
            "speed": 100
        },
        {
            "id": "466a9a0c-4830-4775-8dd0-9acc05ba0b8a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 64,
            "speed": 100
        },
        {
            "id": "d36ba299-0f44-401c-b26b-acb2b77d2384",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 64,
            "speed": 100
        },
        {
            "id": "2640b730-db6e-4b90-9f01-68c9e0d93f5b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "598ff057-99b4-41be-8a76-f39f47e9e523",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 352,
            "speed": 100
        },
        {
            "id": "d0ff0bb9-8cf8-4565-b7ad-cbc26028b4b8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 352,
            "speed": 100
        },
        {
            "id": "4efa7608-ff03-4267-ae74-19f644d6ee27",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 352,
            "speed": 100
        },
        {
            "id": "9420b042-62dd-416d-88d0-d38e9073fdb0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "1f803847-2897-417b-91ed-43db5ea461e6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 64,
            "speed": 100
        },
        {
            "id": "bfd3401e-0a84-433c-af61-e84817ab4a26",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "ed023dd1-025e-4eaf-a104-4f88b722e469",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 160,
            "speed": 100
        },
        {
            "id": "89707b02-b3a9-4457-8af4-b502c1fc58ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 224,
            "speed": 100
        },
        {
            "id": "691501f0-1b63-4ce0-9440-24c39fbbfd46",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 224,
            "speed": 100
        },
        {
            "id": "0c9ba4fe-9eba-44e0-a283-50c526a4eccf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 384,
            "speed": 100
        },
        {
            "id": "e43acc05-25b2-449c-b8fb-e54f2ea64ce4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 384,
            "speed": 100
        },
        {
            "id": "2b8159ab-5ed5-4634-bd2d-e6294a923f7a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 576,
            "speed": 100
        },
        {
            "id": "dfdeabf8-edb2-4c0e-8b28-cbde8c86b728",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 576,
            "speed": 100
        },
        {
            "id": "5e43e14c-cd23-4cd0-91c1-82182834f1e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 512,
            "speed": 100
        },
        {
            "id": "239c5b13-2017-4793-a2eb-82efd8367a80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 512,
            "speed": 100
        },
        {
            "id": "9d226c20-6af2-4975-9de6-31929ad9501f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 512,
            "speed": 100
        },
        {
            "id": "ec8e2b0f-dfd1-4fda-a7a0-b39d72078832",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 512,
            "speed": 100
        },
        {
            "id": "bc919a57-3266-4d42-8e86-b9f4a6048c81",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 480,
            "speed": 100
        },
        {
            "id": "e7da0ad5-c2e1-4e93-ab0c-e433c0a9af9a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 480,
            "speed": 100
        },
        {
            "id": "5215ce5c-ced0-4bc7-876e-03a3ad2e98a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 480,
            "speed": 100
        },
        {
            "id": "0920aaf1-2f2c-4789-9887-24578b3d09a6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 256,
            "speed": 100
        },
        {
            "id": "2954b607-7af4-433f-8878-302966991a0f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 256,
            "speed": 100
        },
        {
            "id": "9c641d83-28f2-49d6-a39f-13ef2f75bee8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 256,
            "speed": 100
        },
        {
            "id": "3c58a9fc-e484-4968-b9c1-6fb8821d758f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 480,
            "speed": 100
        },
        {
            "id": "abd76345-7103-4126-8ffa-d1e058e1d29e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 512,
            "speed": 100
        },
        {
            "id": "5f206abd-ca0f-46f2-8be6-e4bf377a6dfc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 512,
            "speed": 100
        },
        {
            "id": "2eadce79-f7d4-482e-a690-5e854d792194",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 576,
            "speed": 100
        },
        {
            "id": "78bffd4d-e600-4384-9a3e-6ae6d0920e92",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 608,
            "speed": 100
        },
        {
            "id": "01074d30-c2f2-4a56-9476-da05811780ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 704,
            "speed": 100
        },
        {
            "id": "440294e1-720c-4c56-9a49-59695ed65b70",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 704,
            "speed": 100
        },
        {
            "id": "29e7a92f-b47f-4288-8791-88a31b6c4c46",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 704,
            "speed": 100
        },
        {
            "id": "826dbb94-6b29-4e27-8e73-e6138439b351",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 704,
            "speed": 100
        },
        {
            "id": "a82ecabb-0468-4cc4-a0e0-be6081df4fe4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 704,
            "speed": 100
        },
        {
            "id": "261ad5e0-bc35-45de-abe4-356042930b14",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 608,
            "speed": 100
        },
        {
            "id": "87ebbaa5-003b-4352-a523-161539fa91e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 608,
            "speed": 100
        },
        {
            "id": "53c12072-411f-4554-804d-29859f44dcd6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "503e2428-f3a1-47af-9b12-a1647dba83bb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "1a92eb61-a813-4574-9fad-0df40649d63c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "4759565d-25e0-4156-b956-6d3d0069b8b2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "e1731edf-8537-4265-aac5-bcee870c3cf6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "8c16020a-4f77-4da4-bc0b-122b1daef405",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "f90f5a60-dd67-4a54-a81c-7bcf08d99bf8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "f852d2c8-5970-4192-93f7-58236f3f748f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "79d21d11-3b7a-4675-99a9-08d1238097a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "9c24cfa6-397b-435a-9e77-c8a2fdddcd03",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        },
        {
            "id": "e5c6f97b-a665-477c-a0cb-29a28216894b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 448,
            "speed": 100
        }
    ],
    "precision": 20,
    "vsnap": 0
}