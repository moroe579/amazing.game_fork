{
    "id": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb9158d0-ea83-42ba-91f7-f042ed288484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "7387d1f2-bd00-46a5-99c2-ba3cd2c32f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9158d0-ea83-42ba-91f7-f042ed288484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69930a39-42de-4e1e-bdec-4c20a847737c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9158d0-ea83-42ba-91f7-f042ed288484",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "9aca7ba2-4656-4f77-b48b-b03483ff08e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "c4edf359-f760-4112-b40d-fac4797ec5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aca7ba2-4656-4f77-b48b-b03483ff08e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2eab49-3967-443d-a73f-dd6ddd9e01c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aca7ba2-4656-4f77-b48b-b03483ff08e3",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "c981e0bb-dc6a-4ec7-a737-b89105c63d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "7b3c08e9-e64a-40f4-8d72-a935dee529d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c981e0bb-dc6a-4ec7-a737-b89105c63d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec00c8d9-a552-4118-84aa-e647edbceb6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c981e0bb-dc6a-4ec7-a737-b89105c63d33",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "4f0fc364-0863-4aab-b821-2bd6b0ccb58b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "ffe616ae-af05-477a-b082-007f4cd8f872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f0fc364-0863-4aab-b821-2bd6b0ccb58b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99b6e735-e226-406a-bde9-5876a0b6a26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f0fc364-0863-4aab-b821-2bd6b0ccb58b",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "b9e1184c-a335-4a04-80b6-0e3c0596d89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "c024651e-1edd-4306-9f69-f975dafb8c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9e1184c-a335-4a04-80b6-0e3c0596d89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966903a6-129e-40ba-9f2d-0f3dfc2b991f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9e1184c-a335-4a04-80b6-0e3c0596d89e",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "5a31d5fb-2da0-4838-9276-58b98e95da83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "be3b2d0e-28a9-412b-bbda-de5f40c257c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a31d5fb-2da0-4838-9276-58b98e95da83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9aa36a9-6195-4e6b-a7f2-8383bb83f79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a31d5fb-2da0-4838-9276-58b98e95da83",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "ce290cf2-c535-4b7d-bea5-461098ffe497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "8fb5535f-0a63-4ebd-82ba-fe4eb5369505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce290cf2-c535-4b7d-bea5-461098ffe497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f011a75-876c-4d6d-8d94-6072fa45fc6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce290cf2-c535-4b7d-bea5-461098ffe497",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "896ac0b5-018b-44ca-a863-89fb93b02e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "c41d4564-e0f7-4f31-bb44-da3ef9cc2e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896ac0b5-018b-44ca-a863-89fb93b02e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f08ecca-16c9-45fa-b3d6-83d9204c61bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896ac0b5-018b-44ca-a863-89fb93b02e72",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "8907a998-9133-4bba-b5b6-4311dc74a329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "5d6fc1f2-312e-4b47-8c84-95f008c1b30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8907a998-9133-4bba-b5b6-4311dc74a329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b65ba9e-a255-4df5-9dfe-12e9c97bdc16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8907a998-9133-4bba-b5b6-4311dc74a329",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "a3e368ca-b156-44b9-a72c-f085a66bb03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "e5d6794a-a6c6-4419-bee3-f1c1bcb706a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e368ca-b156-44b9-a72c-f085a66bb03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3f17a2-51d6-4032-8685-8dd371c533b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e368ca-b156-44b9-a72c-f085a66bb03a",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "6a3abb78-9f33-4787-a000-f36a198c9114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "4de25ad1-94c3-4ab2-9ced-d361fba819e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3abb78-9f33-4787-a000-f36a198c9114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f900fcb4-6a5d-4ebd-8caa-2257b1a97cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3abb78-9f33-4787-a000-f36a198c9114",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "9be738aa-7704-4b5c-9aca-3a3ef7a14899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "8a44bf08-d330-4003-957b-4102ceab1d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be738aa-7704-4b5c-9aca-3a3ef7a14899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dde81c6-210d-4dc9-a449-6107089be3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be738aa-7704-4b5c-9aca-3a3ef7a14899",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "3a2d8081-ce0a-4c6c-95e9-5359e089fbbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3a52733c-9a0a-4eb5-a953-d631e7fc0589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2d8081-ce0a-4c6c-95e9-5359e089fbbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08202b30-d1d2-40c7-ac09-ae130bdaee5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2d8081-ce0a-4c6c-95e9-5359e089fbbe",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "2027b551-bc86-4d39-9aaf-16d8b0abd6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "55a3c479-d572-47d0-b3e3-a6cc2c08b3f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2027b551-bc86-4d39-9aaf-16d8b0abd6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddecdb77-d120-40ef-b32b-f9faf9146ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2027b551-bc86-4d39-9aaf-16d8b0abd6a2",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "d043073e-84c9-4321-b06b-37056abef8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "0415826e-0e33-4e8f-8103-9b9f87a1d4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d043073e-84c9-4321-b06b-37056abef8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220e4c2d-fb6a-4467-b273-658a8f725c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d043073e-84c9-4321-b06b-37056abef8c7",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "ec4db4a8-11c6-43ef-9515-138e2be81b10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "7c060e42-4171-40d9-a6f6-868169394cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec4db4a8-11c6-43ef-9515-138e2be81b10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bf2c21-5545-4b21-9685-02b2cd33ff99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec4db4a8-11c6-43ef-9515-138e2be81b10",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "04a55dcc-95db-477c-9044-1baf1a2b7de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "89ccf259-679d-4e00-b359-696094ab72f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04a55dcc-95db-477c-9044-1baf1a2b7de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939adaee-dd53-434f-b1a9-3162ae80cce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04a55dcc-95db-477c-9044-1baf1a2b7de1",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "41b730c6-1944-4995-bddc-f0e32caed083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "6889f7ac-3b57-443c-b677-133e1d4fd504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41b730c6-1944-4995-bddc-f0e32caed083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ff2e2f-dcae-479e-adaa-14b9c9c1e07b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41b730c6-1944-4995-bddc-f0e32caed083",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "9bb0f9d8-73e9-4ca1-a29f-4f7b80ac8669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "aa018898-6d2c-4fca-9d7d-38baf9359d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb0f9d8-73e9-4ca1-a29f-4f7b80ac8669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b896208-669d-4641-b8b4-27640454bc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb0f9d8-73e9-4ca1-a29f-4f7b80ac8669",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "fe5ed907-8e3b-4e2f-9d9f-28d79d20d9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3a697d0c-5d1a-497b-bdd3-72123a87d40f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5ed907-8e3b-4e2f-9d9f-28d79d20d9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8264a7-47dd-4f7a-b614-f085687b7ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5ed907-8e3b-4e2f-9d9f-28d79d20d9c1",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "0b4babf8-de97-42ff-af48-c353f51fdcac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "435eba27-ac81-4e4f-bef1-19b81b7831c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b4babf8-de97-42ff-af48-c353f51fdcac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d881548c-d9b2-4a1c-9417-81c6b0038b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b4babf8-de97-42ff-af48-c353f51fdcac",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "b51cf0d6-5717-43c4-b28f-119f7afef890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "ecf9916c-9df1-4864-b3fb-4dbff417cd58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b51cf0d6-5717-43c4-b28f-119f7afef890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad2bfa8-6443-4dc0-8183-be199def0e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b51cf0d6-5717-43c4-b28f-119f7afef890",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "592a094a-f2f3-48db-97da-f5cd4ecb1f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "2b2cee05-c680-4a81-b24e-671d5fda69b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592a094a-f2f3-48db-97da-f5cd4ecb1f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e302c1-2483-4c35-a89c-94816a9b5102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592a094a-f2f3-48db-97da-f5cd4ecb1f20",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "eb90127b-2fda-4766-8b26-5638d32a7f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "c7d4b463-8462-40e8-ba40-13f788004271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb90127b-2fda-4766-8b26-5638d32a7f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21db3dda-f3d3-4a4a-8036-61c652a13a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb90127b-2fda-4766-8b26-5638d32a7f33",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "9a405c61-1572-4c7b-9861-446ed9d2313b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "8d75dca6-1078-4a88-8166-03550dcd9f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a405c61-1572-4c7b-9861-446ed9d2313b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a2b0b4-7c99-44d8-a113-494c571086f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a405c61-1572-4c7b-9861-446ed9d2313b",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "c74f1f61-0af5-4969-9ca9-fe1c9d963902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "2468258f-3f9f-4aff-bde3-dfce8135ae5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74f1f61-0af5-4969-9ca9-fe1c9d963902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b3680d-e63c-4f50-8ac2-f6ecd7e9704c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74f1f61-0af5-4969-9ca9-fe1c9d963902",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "fd82b700-e868-4333-b3f1-cfa5a3c15ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "167495cc-21e7-4b39-be38-cefe81a6dcc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd82b700-e868-4333-b3f1-cfa5a3c15ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad1092e-060b-4692-b3ed-f79fb2f1d2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd82b700-e868-4333-b3f1-cfa5a3c15ce7",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "8d6cd2b6-cc6c-4075-af92-1c0ce4c38657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "744b31e6-9447-4988-abc7-370060a21de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6cd2b6-cc6c-4075-af92-1c0ce4c38657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ab644c-f8fc-4010-911b-4ab463564dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6cd2b6-cc6c-4075-af92-1c0ce4c38657",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "cd521efd-1d3e-4804-9210-89cedea31b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "a2ed78c8-b3a1-4c6f-8610-827518b934bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd521efd-1d3e-4804-9210-89cedea31b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3510cba-c7e2-48e6-baf1-7b2c8e57f5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd521efd-1d3e-4804-9210-89cedea31b31",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "54e5515c-7e86-4822-b8ed-e32affec4c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "7c055833-f1f7-44ec-a7a0-10115a4163f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e5515c-7e86-4822-b8ed-e32affec4c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b480f96f-4341-4f9d-bb6a-53a609c02d7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e5515c-7e86-4822-b8ed-e32affec4c56",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "55375a78-c3cf-42d5-b4a9-b5c6b65c083f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "e55e22a7-31b5-4ba5-b120-ee05164f4934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55375a78-c3cf-42d5-b4a9-b5c6b65c083f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "864e5708-7249-4528-8fc4-eb0df2de80b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55375a78-c3cf-42d5-b4a9-b5c6b65c083f",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "dbc058ed-70fa-4d11-a3ef-d6f013227412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "514875fb-8c20-4ede-b05d-100ef43471d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc058ed-70fa-4d11-a3ef-d6f013227412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3cae4ea-f9d9-4b2b-ae42-38cb9c45b2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc058ed-70fa-4d11-a3ef-d6f013227412",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "4827c1d5-2559-40f0-9f76-9ddbb3c81693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "9868ef11-8845-4c0a-8519-c13c9930a671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4827c1d5-2559-40f0-9f76-9ddbb3c81693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a03b44-b307-4c87-9f7c-7a0f228fdef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4827c1d5-2559-40f0-9f76-9ddbb3c81693",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "7e0dbf22-a687-4084-bf20-122970fbd2dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "67c4b2d1-2ad5-48ba-980c-4b47a09ff8c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0dbf22-a687-4084-bf20-122970fbd2dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6282007e-edc6-4240-868f-59135667bcd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0dbf22-a687-4084-bf20-122970fbd2dc",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "1b94ef36-a6f8-48fa-b8ca-04abbaf9654f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "baaa4f0f-e3ac-462c-9cba-3e32f1b89bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b94ef36-a6f8-48fa-b8ca-04abbaf9654f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f9bb25-5a0c-4379-96ed-1a1be9aa8b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b94ef36-a6f8-48fa-b8ca-04abbaf9654f",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "c51b9a16-7e94-4e95-a30d-f9d46523a1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "97392963-8b1c-4885-97b6-ef2cf95fc8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51b9a16-7e94-4e95-a30d-f9d46523a1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4fec143-5468-4e13-b6be-7b4f12036e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51b9a16-7e94-4e95-a30d-f9d46523a1bf",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "80e08381-ca3a-4bb5-acfb-23b19dc893a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3031ee5d-a59a-4e37-ab4a-24c0862dae0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e08381-ca3a-4bb5-acfb-23b19dc893a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b103da4-6c05-4908-9e84-d454d374e667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e08381-ca3a-4bb5-acfb-23b19dc893a1",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "7775496d-3c7d-498a-8d1b-856e104bc2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "ec9745b0-1335-4746-a644-edaa58076a5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7775496d-3c7d-498a-8d1b-856e104bc2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af5e935-7196-4bbe-9635-42b3047f17f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7775496d-3c7d-498a-8d1b-856e104bc2c7",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "d2cef696-2971-4e63-8779-4c7160036b34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "cea43dcc-fe58-40b2-9a20-b8d4543212ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cef696-2971-4e63-8779-4c7160036b34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd59c9ca-db01-4835-81e8-56396da8b208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cef696-2971-4e63-8779-4c7160036b34",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "05413195-fbce-472b-8e24-ef07b8317b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "de9ed3a9-d30b-4860-b5c4-0e84337db7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05413195-fbce-472b-8e24-ef07b8317b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e00fda-2ba9-4c46-bd16-a49cfbad37e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05413195-fbce-472b-8e24-ef07b8317b9f",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "7c7cd58c-0fb6-4a5c-bf2b-9271459b9b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "6ea11c94-c029-4b98-9f06-f62aa6f01966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c7cd58c-0fb6-4a5c-bf2b-9271459b9b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d4bbe2-70e0-4dd9-bae6-510a99a52dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c7cd58c-0fb6-4a5c-bf2b-9271459b9b88",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "3285b1ce-08e8-45d8-aba4-5f9102da8c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "a406caef-ea19-4045-8478-fdc200b0682c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3285b1ce-08e8-45d8-aba4-5f9102da8c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a08416-6e16-400a-94d2-2c1d68665d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3285b1ce-08e8-45d8-aba4-5f9102da8c2a",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "8be6ad99-692e-4b1a-afa9-906cafbaa3ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "aa079f94-129f-42a7-bee9-069ecd1859c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be6ad99-692e-4b1a-afa9-906cafbaa3ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7385c54a-2dfb-45dd-9509-602654ff4e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be6ad99-692e-4b1a-afa9-906cafbaa3ba",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "b9cccd65-06c3-4cd6-aa7c-2fde107eb95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "dac201d3-277a-40c6-8397-d6700ca27d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cccd65-06c3-4cd6-aa7c-2fde107eb95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4b48f6-170e-46fc-8f08-53b1b4514823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cccd65-06c3-4cd6-aa7c-2fde107eb95a",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "151ce72e-6b05-449b-ae87-8cae282a60d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "812a4a9f-6c65-41cc-aeeb-ca439986671c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "151ce72e-6b05-449b-ae87-8cae282a60d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "170e3a22-efe3-49bf-adca-ad104b492055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151ce72e-6b05-449b-ae87-8cae282a60d3",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "32aad57c-a095-4395-b57e-2da31815ebbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3ad27ef0-7c05-4e65-8992-64b2716db6e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32aad57c-a095-4395-b57e-2da31815ebbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c19e00-86be-4238-8eb7-3d7be5a5a1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32aad57c-a095-4395-b57e-2da31815ebbb",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "060e6d2b-939d-482b-ae0e-76bfbc1213aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "0b60cf23-d076-46df-bf36-31a190c8fc70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060e6d2b-939d-482b-ae0e-76bfbc1213aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ed2d41-065f-4c4d-9815-747028e4ee1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060e6d2b-939d-482b-ae0e-76bfbc1213aa",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "19e15dfa-4f71-421b-b63e-fca17951b7d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "52d3587e-6677-4fa7-84fa-ec4cfddc9907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19e15dfa-4f71-421b-b63e-fca17951b7d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f0e52e-7e9b-4c3c-80b4-8b9a3aa95ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19e15dfa-4f71-421b-b63e-fca17951b7d8",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "ebac8767-ee1e-4960-8409-8b4f232ca75a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "f55d13a9-f4f6-4609-9db9-4b360f2aaf88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebac8767-ee1e-4960-8409-8b4f232ca75a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63a01a5-e585-47f7-b743-9c967c96c4ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebac8767-ee1e-4960-8409-8b4f232ca75a",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "d732f801-c5d1-4d97-9a5d-c8cb0bddf748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "e3aa273f-d9d8-4b57-a1b1-1d92c675d291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d732f801-c5d1-4d97-9a5d-c8cb0bddf748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ee2b7c-a4a1-4e18-9612-0e0cf8fcd20e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d732f801-c5d1-4d97-9a5d-c8cb0bddf748",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "e54d6eeb-12c1-4a68-916e-3eb41161cf17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "8d188374-4e45-462a-9890-280eee37a08a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54d6eeb-12c1-4a68-916e-3eb41161cf17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e55405-6a4e-45cb-8b38-e2f9f4970c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54d6eeb-12c1-4a68-916e-3eb41161cf17",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "692c649e-3b2c-43bd-a337-73adeb46b986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "ca5ea7ef-36a3-48f5-bc73-06ce040949d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692c649e-3b2c-43bd-a337-73adeb46b986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7415b2-d5c9-4172-8eb4-8ad305c81c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692c649e-3b2c-43bd-a337-73adeb46b986",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "8b801609-9b03-45a6-902e-b2c6e98cdf8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3fb91de6-3b8a-4c22-9e90-6340b745623b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b801609-9b03-45a6-902e-b2c6e98cdf8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d086cedf-68f4-4ebe-8552-0b4de1e8ddb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b801609-9b03-45a6-902e-b2c6e98cdf8d",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "aabb995f-317b-4f5a-b634-610724c2e123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "e9bf8ee7-106b-42d7-9922-6930d51697e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabb995f-317b-4f5a-b634-610724c2e123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9663e168-48d0-4a63-91a7-21c4f1fd99b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabb995f-317b-4f5a-b634-610724c2e123",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "4a930162-0223-4bc9-86c0-a60f950da29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "be06ac30-b8af-4650-b1c6-f327551e8e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a930162-0223-4bc9-86c0-a60f950da29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c223007-ffda-4056-a583-d22be876c9b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a930162-0223-4bc9-86c0-a60f950da29d",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "2ed38346-8f59-4819-bf0d-2dfe9089197a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3b3ecd2f-6d0c-4ac1-9a47-023faa122a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed38346-8f59-4819-bf0d-2dfe9089197a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fca3c03-6eeb-4d30-bfc2-eb4c00ddc781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed38346-8f59-4819-bf0d-2dfe9089197a",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "5ec05425-d304-4048-8ac5-4e14f586e01e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "e8f6be7f-0d79-4b65-adb9-37f905819861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ec05425-d304-4048-8ac5-4e14f586e01e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45370823-71b3-4b52-a425-f32e0656ca78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ec05425-d304-4048-8ac5-4e14f586e01e",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "e0fcecc7-ac0e-4080-8067-8b4b19b20d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "3174284d-bf25-4d1e-b2db-2b270d84acf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fcecc7-ac0e-4080-8067-8b4b19b20d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f594af4-2551-403e-b466-389104d90d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fcecc7-ac0e-4080-8067-8b4b19b20d03",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "79e4be22-1217-4855-aed8-82f477c66ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "2e238ea1-0f81-4d37-9561-b8960965dae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79e4be22-1217-4855-aed8-82f477c66ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be220110-c729-4de1-970f-9ed379919d3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79e4be22-1217-4855-aed8-82f477c66ee2",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        },
        {
            "id": "44879162-bb24-430c-8b4a-b479213ad042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "compositeImage": {
                "id": "6ba070eb-78ef-4cab-be85-da4bf7ffbc71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44879162-bb24-430c-8b4a-b479213ad042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8d7252-f7a2-4ae7-b405-cd5a61118266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44879162-bb24-430c-8b4a-b479213ad042",
                    "LayerId": "f594387c-cf57-4858-a30c-667ca6cea1ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f594387c-cf57-4858-a30c-667ca6cea1ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}