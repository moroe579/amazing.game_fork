{
    "id": "44d488ff-9583-40d3-88e7-3149ed495921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9a10f2b-447e-49fc-8134-06233b3e8f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "26f30fea-e722-449a-9e6d-0cb52b351e9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a10f2b-447e-49fc-8134-06233b3e8f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d99ebd-1394-4f42-b063-63a9f48fa3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a10f2b-447e-49fc-8134-06233b3e8f61",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "ea232a1c-f65c-474b-a45e-c0eab8dcc8c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "d5dfe01a-1925-48fd-92ec-bbb37cc2dc69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea232a1c-f65c-474b-a45e-c0eab8dcc8c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "739106e9-8134-45f9-8235-5a9730300026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea232a1c-f65c-474b-a45e-c0eab8dcc8c4",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "7353bd49-0bf3-4fac-8791-2aadf84b7d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "5062e218-f3d0-4d64-a82a-cd2d41ee5026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7353bd49-0bf3-4fac-8791-2aadf84b7d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af8fb40-c6c2-4cd4-b1fc-1d49c2ae6039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7353bd49-0bf3-4fac-8791-2aadf84b7d99",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "4f7682c6-d39c-4eb2-a1eb-7436613436ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "0054d784-c880-407c-92e7-93549740a327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7682c6-d39c-4eb2-a1eb-7436613436ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2134ccf-2e3b-41dd-b5bc-c0c9c1457d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7682c6-d39c-4eb2-a1eb-7436613436ae",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "46aa1728-5b2c-40e9-9a5a-de603661f0fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "b099cb86-a98a-49fc-a9d2-47545c57b3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46aa1728-5b2c-40e9-9a5a-de603661f0fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1039554-6814-4310-b110-6dbf1b9690d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46aa1728-5b2c-40e9-9a5a-de603661f0fb",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "83c0b37f-35cc-4af7-b006-9966bcd78ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "870f98ad-dbff-47e1-8944-32b2487433d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c0b37f-35cc-4af7-b006-9966bcd78ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6687624f-4eca-4f8a-824b-8e7c6dd2a08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c0b37f-35cc-4af7-b006-9966bcd78ab0",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "5514becc-5119-434d-a1b1-a32756795ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "3441e01f-dbb1-4874-85b3-f9bbb07a6190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5514becc-5119-434d-a1b1-a32756795ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9b4930-f999-4816-a193-84e45035a4e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5514becc-5119-434d-a1b1-a32756795ea3",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        },
        {
            "id": "7cf44c3b-9f58-4ba5-8e19-e18fbe99f5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "compositeImage": {
                "id": "ecf0e06c-72e2-46a6-bc87-d9041708bddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf44c3b-9f58-4ba5-8e19-e18fbe99f5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89046956-1695-4639-9470-8a281a32297b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf44c3b-9f58-4ba5-8e19-e18fbe99f5ec",
                    "LayerId": "8e419dba-84d7-4139-b465-200eba26b075"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "8e419dba-84d7-4139-b465-200eba26b075",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}