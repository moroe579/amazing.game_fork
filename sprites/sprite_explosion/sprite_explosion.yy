{
    "id": "a3629074-ad35-4077-bbcb-01738a4eea85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da7f3143-148c-40e3-b2da-d472e2114375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "967eb201-16bc-41da-a389-92f98de03c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da7f3143-148c-40e3-b2da-d472e2114375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74441a23-02d0-4ecb-8b97-a72b1e1fe2a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da7f3143-148c-40e3-b2da-d472e2114375",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "3f560ceb-2d27-48d6-8efa-cbbcaeb5643e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "76068fae-57ee-4c40-8370-4182a558e4d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f560ceb-2d27-48d6-8efa-cbbcaeb5643e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d489a62e-53c3-4e0b-a2d7-daf18a9c38c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f560ceb-2d27-48d6-8efa-cbbcaeb5643e",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "f52fdda6-9df6-427a-a816-2b5e7bdd8441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "3e8587e0-0ecb-434b-b2a5-0a62179226a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52fdda6-9df6-427a-a816-2b5e7bdd8441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282dd903-b14c-4af4-bb9b-c674e39ee844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52fdda6-9df6-427a-a816-2b5e7bdd8441",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "fc9b263a-887d-4489-874a-7cca1df38aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "744e0088-0b00-4989-a905-bf95602ca70e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc9b263a-887d-4489-874a-7cca1df38aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2870547d-83ca-4120-83e5-8b9f9305223f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc9b263a-887d-4489-874a-7cca1df38aa8",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "3977bfbc-cb40-4766-9e75-03555b7e7bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "c6138253-e882-4c8b-896d-99d643d167a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3977bfbc-cb40-4766-9e75-03555b7e7bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8560e0d9-7ff5-4f02-afd6-152a0375adbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3977bfbc-cb40-4766-9e75-03555b7e7bf9",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "b96650e5-b75a-488e-bf65-727f18e67b16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "f7bd0955-2eda-40d8-88a6-d4cc4964e8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96650e5-b75a-488e-bf65-727f18e67b16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f38a181-110d-4e7c-9fc5-75e0f873751e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96650e5-b75a-488e-bf65-727f18e67b16",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        },
        {
            "id": "4774bdaa-cfbf-4d4b-984e-859f58206a77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "compositeImage": {
                "id": "a1d48b95-38ae-4c51-b3b2-47cc0ac0ece6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4774bdaa-cfbf-4d4b-984e-859f58206a77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34eb710-2d9e-456d-a760-a51934970354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4774bdaa-cfbf-4d4b-984e-859f58206a77",
                    "LayerId": "70752bd8-b316-476c-afae-070ddbf7062a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "70752bd8-b316-476c-afae-070ddbf7062a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}