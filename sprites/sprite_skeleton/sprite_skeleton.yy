{
    "id": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_skeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a233c3a4-150d-4120-8a1c-9f623c009058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "0fcb44b5-384f-41e3-a219-61b1e7c8a16e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a233c3a4-150d-4120-8a1c-9f623c009058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edb0d4a-5005-4479-bf8d-566dbde496c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a233c3a4-150d-4120-8a1c-9f623c009058",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "fd39f2cd-0a9f-4068-a254-0705176ad177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "da8a8b65-5188-4a8a-82bc-b5ffb9d4b95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd39f2cd-0a9f-4068-a254-0705176ad177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28117462-2309-48a5-ac87-79a426eec9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd39f2cd-0a9f-4068-a254-0705176ad177",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "2b0b8297-8618-4c5a-8922-5a4a88cc1393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "1bd5639f-11bd-4ef3-a42c-95a73f59e455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0b8297-8618-4c5a-8922-5a4a88cc1393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61249614-5de2-48da-8d7b-0a23c61b25e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0b8297-8618-4c5a-8922-5a4a88cc1393",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "fc3b6df1-c101-4e20-a997-9a4c73acefe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "36554549-9475-4a15-a76a-15b62138a570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3b6df1-c101-4e20-a997-9a4c73acefe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17c22ba7-f85b-4a7f-bf5c-8fa6820c4a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3b6df1-c101-4e20-a997-9a4c73acefe2",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "b72f179d-725c-47ca-a922-6e01d3357c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "d10cf4e9-dcef-4b6d-87e5-dcec42785d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72f179d-725c-47ca-a922-6e01d3357c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49231831-699e-4747-99e1-1e8065cc3833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72f179d-725c-47ca-a922-6e01d3357c13",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "22c34746-0e13-4446-8747-667049ac93ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "09f1dc0a-c4ee-4f6b-9d6f-e7acd3c94fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22c34746-0e13-4446-8747-667049ac93ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e651b19-f6c9-4dbf-aaa2-9fb74da6434a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22c34746-0e13-4446-8747-667049ac93ca",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "2856e1a7-5b9b-4196-a821-55f66a66007a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "7e6fb899-c097-4bfa-9725-6ca568b68f15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2856e1a7-5b9b-4196-a821-55f66a66007a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec81a529-d89c-4d01-850e-533a18c04976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2856e1a7-5b9b-4196-a821-55f66a66007a",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "6b891743-b232-4e41-9f40-eb99400f0c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "ea421d48-4bfb-4a35-a2a1-8d658947e9b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b891743-b232-4e41-9f40-eb99400f0c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f997775d-a52e-4262-8098-d0cba3062a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b891743-b232-4e41-9f40-eb99400f0c87",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "ae77d6c5-f0d4-4be3-899e-2d0d29af241d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "7ca51505-c2cf-4b11-a30f-fc79a9d17b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae77d6c5-f0d4-4be3-899e-2d0d29af241d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b94cd0-d58d-41f4-8c51-7e0fc1713ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae77d6c5-f0d4-4be3-899e-2d0d29af241d",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        },
        {
            "id": "63c918a6-028d-4870-a51d-075ef9de03ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "compositeImage": {
                "id": "a713c00c-f127-411e-8784-0f67e3e3259e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c918a6-028d-4870-a51d-075ef9de03ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c458a7-bf5a-41cf-b4ae-d244843face6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c918a6-028d-4870-a51d-075ef9de03ad",
                    "LayerId": "4bb351fd-8193-474e-acd9-b7f742f3823a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4bb351fd-8193-474e-acd9-b7f742f3823a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}