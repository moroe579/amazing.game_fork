{
    "id": "07493c15-420b-4181-a389-378f1cbcf683",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_red_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b41825d7-cd8b-4fb0-b351-9e84f12c3ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07493c15-420b-4181-a389-378f1cbcf683",
            "compositeImage": {
                "id": "e7627837-6bbe-4ae9-82ba-54445558057c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41825d7-cd8b-4fb0-b351-9e84f12c3ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38d458e-189d-490b-b1e2-3adadbce62b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41825d7-cd8b-4fb0-b351-9e84f12c3ca5",
                    "LayerId": "33d6a613-ae7f-4614-9a52-99c8bf5617a2"
                }
            ]
        },
        {
            "id": "ff31865d-4c44-4af7-b7d0-9af180418410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07493c15-420b-4181-a389-378f1cbcf683",
            "compositeImage": {
                "id": "31682b22-a665-4222-b370-72a0cbe6c508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff31865d-4c44-4af7-b7d0-9af180418410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf8b68e-8be6-4da4-aaa6-47fb5b0f9f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff31865d-4c44-4af7-b7d0-9af180418410",
                    "LayerId": "33d6a613-ae7f-4614-9a52-99c8bf5617a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33d6a613-ae7f-4614-9a52-99c8bf5617a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07493c15-420b-4181-a389-378f1cbcf683",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}