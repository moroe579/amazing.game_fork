{
    "id": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8878466-565d-4e56-9fc6-fd3ac05106eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "compositeImage": {
                "id": "fd6a4f39-103a-4db2-941f-a60cc57c46e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8878466-565d-4e56-9fc6-fd3ac05106eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1672bf5-cff8-43f2-aca7-0c0d6ff6a2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8878466-565d-4e56-9fc6-fd3ac05106eb",
                    "LayerId": "94ac3eaf-3071-4673-b087-c3b028facf8a"
                }
            ]
        },
        {
            "id": "a441a09e-af57-409b-9104-60243e4b452f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "compositeImage": {
                "id": "2ff7979f-96ed-4d7d-80e3-65a12b70e034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a441a09e-af57-409b-9104-60243e4b452f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d791a647-8e51-47df-be76-8708720f1060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a441a09e-af57-409b-9104-60243e4b452f",
                    "LayerId": "94ac3eaf-3071-4673-b087-c3b028facf8a"
                }
            ]
        },
        {
            "id": "3aea2763-a445-422b-a432-3db54d60e23e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "compositeImage": {
                "id": "d22e080f-92f9-46d0-942e-a705e7d4b873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aea2763-a445-422b-a432-3db54d60e23e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8562606a-4deb-43e4-b208-1bf6946cfb9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aea2763-a445-422b-a432-3db54d60e23e",
                    "LayerId": "94ac3eaf-3071-4673-b087-c3b028facf8a"
                }
            ]
        },
        {
            "id": "0f6abd0a-bbb1-4a12-9f1c-fb718cb5d4c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "compositeImage": {
                "id": "98247ce0-97f8-4159-8750-188372ff3de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6abd0a-bbb1-4a12-9f1c-fb718cb5d4c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d5f537-5f45-495c-9d75-7d55a8209c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6abd0a-bbb1-4a12-9f1c-fb718cb5d4c6",
                    "LayerId": "94ac3eaf-3071-4673-b087-c3b028facf8a"
                }
            ]
        },
        {
            "id": "e1969d47-cd85-4a4a-83a0-f3efb5d19375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "compositeImage": {
                "id": "74c2484a-3101-4509-bc45-2d67dc4a7e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1969d47-cd85-4a4a-83a0-f3efb5d19375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07fda0a1-4735-44a0-b88c-f8aa686372d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1969d47-cd85-4a4a-83a0-f3efb5d19375",
                    "LayerId": "94ac3eaf-3071-4673-b087-c3b028facf8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "94ac3eaf-3071-4673-b087-c3b028facf8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}