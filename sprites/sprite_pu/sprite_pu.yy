{
    "id": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_pu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc304382-6507-479d-8e91-0cb47978a55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "097e204b-852e-4546-9d29-3ff588b70c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc304382-6507-479d-8e91-0cb47978a55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f511302-f80f-4e06-9a8a-9cbd4063f010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc304382-6507-479d-8e91-0cb47978a55a",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "8b80e373-1f02-455d-9e56-ab562b596d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "ed5d18b8-e178-4224-95d0-777f5886237c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b80e373-1f02-455d-9e56-ab562b596d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31661a9e-2206-4719-85e9-c1c054f99121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b80e373-1f02-455d-9e56-ab562b596d06",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "2498e7c6-d267-4a13-a616-e8fc0e220538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "b5259dae-db00-4cc3-9a72-a0c0baa15367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2498e7c6-d267-4a13-a616-e8fc0e220538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e97e41-2b2b-486a-8372-356e9abba68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2498e7c6-d267-4a13-a616-e8fc0e220538",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "2d33d2dd-c363-43eb-9a95-b2834113c5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "58fc9ebc-7c09-4d0c-9a9c-a6728b58ef7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d33d2dd-c363-43eb-9a95-b2834113c5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c289ab-6376-4f0e-b356-4c5476b17dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d33d2dd-c363-43eb-9a95-b2834113c5e8",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "f3e13228-e1e2-4f47-a72f-8a5da2952f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "fcd23c38-cf3a-4b08-a7ab-98cc95d6832d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e13228-e1e2-4f47-a72f-8a5da2952f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88648619-14ce-4770-854b-18dc55562f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e13228-e1e2-4f47-a72f-8a5da2952f03",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "5b681137-d776-4491-aee0-f7af9a450646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "c7d6af4b-2e51-4482-9737-2b4364a26d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b681137-d776-4491-aee0-f7af9a450646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60a346db-e061-4c58-a390-0e25ea83556b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b681137-d776-4491-aee0-f7af9a450646",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "1ab175ab-d263-4e1e-a48c-735066407c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "bdc63d43-7546-441c-8c23-5122e0df3dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ab175ab-d263-4e1e-a48c-735066407c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "339048f8-18f8-4347-b247-1df807a3587f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ab175ab-d263-4e1e-a48c-735066407c89",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "45963e1c-95d4-4b6c-806b-462cbd0f2a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "8d680f45-cb21-4bb9-8cc9-8a523f9778e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45963e1c-95d4-4b6c-806b-462cbd0f2a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ef1086-5bcd-4e8d-bc01-cbafdc8c5e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45963e1c-95d4-4b6c-806b-462cbd0f2a20",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "a4df2f41-18f0-4d16-b4ec-b0643db44d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "a3e0bb51-f610-4ca0-a237-e4536338d9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4df2f41-18f0-4d16-b4ec-b0643db44d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f455fae-74e9-497b-838e-41df35b0de80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4df2f41-18f0-4d16-b4ec-b0643db44d49",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "0d61b889-5410-4488-b691-3e0d9c7786a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "90d781e2-8029-4e5c-b89c-9da0e46cc8af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d61b889-5410-4488-b691-3e0d9c7786a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00f9bdc5-0c51-4f71-b3e8-7b353a22ff57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d61b889-5410-4488-b691-3e0d9c7786a6",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "7930a4ae-18ea-4821-b0fc-a01377c5d339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "4a9cbdce-37eb-4790-8b33-827842aed93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7930a4ae-18ea-4821-b0fc-a01377c5d339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b98a02a-02c0-4a34-b289-e3d7b8f8ddc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7930a4ae-18ea-4821-b0fc-a01377c5d339",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "7f0ed888-d75a-443e-ab8e-bca52a1baca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "e392d262-4fdb-404f-8f08-fa727c0c149e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f0ed888-d75a-443e-ab8e-bca52a1baca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e01f518-ab3d-4b90-b461-081049d09848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f0ed888-d75a-443e-ab8e-bca52a1baca2",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "e5e9928e-36d0-42e2-97e7-66e424689e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "7d226dd8-b015-496e-bb50-6b46a19acdb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e9928e-36d0-42e2-97e7-66e424689e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1f49b4-42e6-4fdf-941c-d7d0ef7b1ad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e9928e-36d0-42e2-97e7-66e424689e5d",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "dea41a44-9221-4283-8bd8-1b447c4249dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "347ae053-efb4-4bd5-8b9f-c52c6d7d2bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea41a44-9221-4283-8bd8-1b447c4249dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2509b75f-03dc-4001-b97a-0122e81309c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea41a44-9221-4283-8bd8-1b447c4249dc",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "3ee9fd52-f351-4c7d-9301-11e85e7d5edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "15ac8686-c613-4141-9aaf-3c886f700665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee9fd52-f351-4c7d-9301-11e85e7d5edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d988ca1-78fd-42a2-b3cb-0bc1c15ec189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee9fd52-f351-4c7d-9301-11e85e7d5edb",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "9285fe15-b6f0-4313-8986-cb812ee0a95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "2552e436-dea3-4d8c-abe2-5106b5039701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9285fe15-b6f0-4313-8986-cb812ee0a95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be5fe8b-c871-48ce-bb3e-cdde0d17ea54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9285fe15-b6f0-4313-8986-cb812ee0a95b",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "6d14819d-6ed0-442d-b3cc-ac562deac397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "2a20862d-2e36-416c-9ac8-2ab273347e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d14819d-6ed0-442d-b3cc-ac562deac397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf4ff8b-34d4-4cf2-bb11-af7e7e475870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d14819d-6ed0-442d-b3cc-ac562deac397",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "5e726832-ce84-4552-8b3d-58c009b9a177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "9a6df019-726b-4e89-815a-be0eeb84bc24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e726832-ce84-4552-8b3d-58c009b9a177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38beef01-39f1-4a16-a733-c3aa94cbb0c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e726832-ce84-4552-8b3d-58c009b9a177",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "75d0cd19-88ee-4416-ae27-eb5871aa9334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "5e3c2d4e-3a43-4107-acd1-71fcabfb9841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d0cd19-88ee-4416-ae27-eb5871aa9334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9925580-215c-476b-9d9d-bbf347c58267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d0cd19-88ee-4416-ae27-eb5871aa9334",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "537ea59c-45c2-468f-8976-bbf72e8dea86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "d087c97f-2345-4d22-bce8-8a2b3731d427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537ea59c-45c2-468f-8976-bbf72e8dea86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85e0757-e760-4ec1-9b7f-68b6ca35e824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537ea59c-45c2-468f-8976-bbf72e8dea86",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "c8c27429-0f8d-4be6-8fef-f50b07fd8119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "6d1a402a-59f5-43e4-92e0-a357db02fe27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c27429-0f8d-4be6-8fef-f50b07fd8119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e7cff6-a336-4f12-b3d9-c1c0b6d5b26e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c27429-0f8d-4be6-8fef-f50b07fd8119",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "22e192e7-a078-4f18-adb4-e44b527c40ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "589a6d5c-0809-4bd6-be69-6c18971b89bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e192e7-a078-4f18-adb4-e44b527c40ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0251060d-4eab-422d-bdd9-145ded9759d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e192e7-a078-4f18-adb4-e44b527c40ff",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "e035d694-b1b1-44cd-97eb-fcd5a1efdf3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "a996abcf-fabc-4e2f-bf80-a1361c35efd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e035d694-b1b1-44cd-97eb-fcd5a1efdf3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46ffa13d-d692-4b48-a01d-0e2106608d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e035d694-b1b1-44cd-97eb-fcd5a1efdf3b",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "115320e6-3930-4a61-b6a7-8a231e151729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "1f0325c2-45a8-4b94-af66-2e9e7bc8f246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115320e6-3930-4a61-b6a7-8a231e151729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c53c9af-5187-4756-a232-0d7192c9cb81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115320e6-3930-4a61-b6a7-8a231e151729",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "8d5b79ca-8ef1-45aa-9ecc-b8da1a27dd63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "4a0ef8be-2d63-4fcb-85d0-493c76c0c978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5b79ca-8ef1-45aa-9ecc-b8da1a27dd63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f56201c-5a98-4adc-b3b8-79645d7cbc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5b79ca-8ef1-45aa-9ecc-b8da1a27dd63",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "2482ad85-26ee-4f3c-a4e3-a9eebba4ada1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "0141bf88-b4a3-46ba-90e2-aec0f0d9ebfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2482ad85-26ee-4f3c-a4e3-a9eebba4ada1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675d6ec2-299c-4fe6-a84b-a09913413b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2482ad85-26ee-4f3c-a4e3-a9eebba4ada1",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        },
        {
            "id": "b900104e-70b2-4898-ab5d-81d7ac29b5af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "compositeImage": {
                "id": "ec8adb04-ea6c-47f9-894e-b0c6416b4c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b900104e-70b2-4898-ab5d-81d7ac29b5af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa71078e-184a-40c0-b5ba-e956247b31fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b900104e-70b2-4898-ab5d-81d7ac29b5af",
                    "LayerId": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e3f3142-b7b5-4460-aac9-f3cb7b97c3d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}