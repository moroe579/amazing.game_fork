{
    "id": "318c1cba-205f-4650-ad2d-00d115f86af8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95c73bd7-2c7b-4615-827f-d5c01646ed7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "318c1cba-205f-4650-ad2d-00d115f86af8",
            "compositeImage": {
                "id": "23dae741-255e-4f53-bbf6-10678904adae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95c73bd7-2c7b-4615-827f-d5c01646ed7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "093f577d-5902-4f40-a2f7-31497671920b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95c73bd7-2c7b-4615-827f-d5c01646ed7a",
                    "LayerId": "e739d83a-cb1d-438a-91fe-14617717c21c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e739d83a-cb1d-438a-91fe-14617717c21c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "318c1cba-205f-4650-ad2d-00d115f86af8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 13
}