{
    "id": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fd58928-9618-4d54-bf44-d01c2dc8b061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
            "compositeImage": {
                "id": "3b95b12b-1d8d-4291-bd62-9a5f9909f8f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd58928-9618-4d54-bf44-d01c2dc8b061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c078f2d-b935-428b-989f-f9363eb41358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd58928-9618-4d54-bf44-d01c2dc8b061",
                    "LayerId": "9fd3f9d2-051b-45b7-824e-ec982129bfa7"
                }
            ]
        },
        {
            "id": "388bd25c-43e9-42a4-a67f-06f94080b936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
            "compositeImage": {
                "id": "75e93aaa-6deb-4be5-ad09-801c0a5a6265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388bd25c-43e9-42a4-a67f-06f94080b936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5516188e-1449-453f-86d1-f03c0f38e809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388bd25c-43e9-42a4-a67f-06f94080b936",
                    "LayerId": "9fd3f9d2-051b-45b7-824e-ec982129bfa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fd3f9d2-051b-45b7-824e-ec982129bfa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}