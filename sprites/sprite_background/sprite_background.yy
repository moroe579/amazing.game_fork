{
    "id": "f4b46501-8583-41db-851d-483d0a2f1158",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4305c13-70da-42b2-81fc-3512a85b4a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b46501-8583-41db-851d-483d0a2f1158",
            "compositeImage": {
                "id": "925d1f45-0e6f-414a-b071-8a3675dd47dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4305c13-70da-42b2-81fc-3512a85b4a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e23bcb06-6fa8-4292-a8bc-ff25e01afe87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4305c13-70da-42b2-81fc-3512a85b4a4f",
                    "LayerId": "53168873-6140-48ed-9c9c-16435fc598b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "53168873-6140-48ed-9c9c-16435fc598b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4b46501-8583-41db-851d-483d0a2f1158",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}