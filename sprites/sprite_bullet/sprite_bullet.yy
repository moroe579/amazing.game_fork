{
    "id": "fd50b019-0133-4971-975e-13c9b58dc274",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "797f9e8f-b535-4f70-87bd-7dd58bc29c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd50b019-0133-4971-975e-13c9b58dc274",
            "compositeImage": {
                "id": "219056f5-b68e-4afb-bac3-a569e515e70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797f9e8f-b535-4f70-87bd-7dd58bc29c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e1b6f2-2ca1-4368-ab88-5b41cbbabcfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797f9e8f-b535-4f70-87bd-7dd58bc29c7a",
                    "LayerId": "da30828b-4e87-41e4-b29b-326c31b28a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da30828b-4e87-41e4-b29b-326c31b28a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd50b019-0133-4971-975e-13c9b58dc274",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}