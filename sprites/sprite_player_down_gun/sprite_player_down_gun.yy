{
    "id": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37a7e401-4aa2-42f0-81c2-411ebe0e5c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "848d2c62-ab4c-4413-b765-d1d546debdd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a7e401-4aa2-42f0-81c2-411ebe0e5c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5220c43f-1afd-4ffb-b167-cddfcbf13264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a7e401-4aa2-42f0-81c2-411ebe0e5c23",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "9ef2e4d4-9adb-4948-b4cb-a3e9010ebb0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "bbdb7859-01df-463d-8c30-ed1f64bc2582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef2e4d4-9adb-4948-b4cb-a3e9010ebb0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3adc039-78cb-4402-ae9e-47b0a02b896b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef2e4d4-9adb-4948-b4cb-a3e9010ebb0c",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "848b4186-6ca4-4cc1-9245-7754760a583f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "b37bcb0a-0d93-4c73-9864-8fb3585a3c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "848b4186-6ca4-4cc1-9245-7754760a583f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d664fd-d455-4a97-8440-6d6e246b5728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "848b4186-6ca4-4cc1-9245-7754760a583f",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "907ef8aa-3e21-4bfa-99fb-c911f40928d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "89ebe356-a1d4-40c4-8f99-fefd8f745a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907ef8aa-3e21-4bfa-99fb-c911f40928d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a259398c-d94e-4d1c-b313-f486948eb7a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907ef8aa-3e21-4bfa-99fb-c911f40928d0",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "dd2f0741-8d5f-4eb7-99f3-9150ad8a0da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "4ef1cbc9-2b69-4be2-9559-c83ed7afc1e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2f0741-8d5f-4eb7-99f3-9150ad8a0da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "807ba4a5-c312-44ec-9580-77c5d4dc44dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2f0741-8d5f-4eb7-99f3-9150ad8a0da3",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "8d041489-40d7-4fbd-aed9-01d2cc1c05f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "cb1a16a0-b5bf-4821-8c2d-2d58bdd9ebcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d041489-40d7-4fbd-aed9-01d2cc1c05f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce23189-9984-44d5-9b03-cfd0b6b26781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d041489-40d7-4fbd-aed9-01d2cc1c05f9",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "fe069407-f45d-49cb-8961-ad3e6651e40b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "900df992-2344-4f84-9c40-a19e711cc5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe069407-f45d-49cb-8961-ad3e6651e40b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b395fa-dbf3-4510-ac81-234ad578edb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe069407-f45d-49cb-8961-ad3e6651e40b",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        },
        {
            "id": "982827ad-0ec9-4762-85c6-c676460f68ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "compositeImage": {
                "id": "182cc171-f460-4a32-86a0-ef1d8693c57f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982827ad-0ec9-4762-85c6-c676460f68ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ecb40e-0046-4e4e-9267-5693a0dd37d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982827ad-0ec9-4762-85c6-c676460f68ac",
                    "LayerId": "4cf78529-780a-4f9c-ae65-7bc3a481df8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "4cf78529-780a-4f9c-ae65-7bc3a481df8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5309ea9d-44ac-4c5e-8749-5e808e76c6b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}