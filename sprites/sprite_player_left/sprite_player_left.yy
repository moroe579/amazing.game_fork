{
    "id": "bd06f3cd-02be-4e40-b284-d752759bba09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99a5b837-2d56-45a5-93ac-98d7dfbd41ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "e194f4c7-a818-4be8-b3ab-a322bec486b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a5b837-2d56-45a5-93ac-98d7dfbd41ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f21746-d390-4604-addc-a7d29ce46b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a5b837-2d56-45a5-93ac-98d7dfbd41ac",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "66bba448-ce9f-4ce6-9c6b-9167c0494581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "1b2aa3ec-8415-45fe-ac9f-52e010837705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66bba448-ce9f-4ce6-9c6b-9167c0494581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8c30e5-374f-41c2-98da-7e707afbb5ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66bba448-ce9f-4ce6-9c6b-9167c0494581",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "6fe042d5-b81f-4c67-98ef-9e43a7bce4ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "4ee49514-4bb3-4321-b39e-6b32adc84ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe042d5-b81f-4c67-98ef-9e43a7bce4ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f0f4ad-5cb5-4130-b5fa-7f8986d88ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe042d5-b81f-4c67-98ef-9e43a7bce4ac",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "5d45a14d-8b24-4252-91fe-55f26a922d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "1f43022a-5247-49e4-a116-4ebac9283b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d45a14d-8b24-4252-91fe-55f26a922d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "052d691d-9656-4e3b-80af-51c34c000f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d45a14d-8b24-4252-91fe-55f26a922d55",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "755a1cbc-486e-4cc4-9153-dd04d8c435d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "e3f8edd7-83c0-4c65-9d75-bca3cbd8dca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "755a1cbc-486e-4cc4-9153-dd04d8c435d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7a6aa9-8cb8-473a-8a5a-b79f6c04333e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "755a1cbc-486e-4cc4-9153-dd04d8c435d7",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "67411f7d-fbf3-4d82-9196-e6e84f112384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "4f2d1cba-4098-470e-abd0-c9bfa5ea757f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67411f7d-fbf3-4d82-9196-e6e84f112384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec27bfae-c7ac-4926-8854-472376d96ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67411f7d-fbf3-4d82-9196-e6e84f112384",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "0ad8239f-531f-4509-8821-9de27060c9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "c221937a-2494-44bf-b577-79bc63f5ceb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad8239f-531f-4509-8821-9de27060c9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5170e8-778b-4e2b-b6d2-394e99a8f754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad8239f-531f-4509-8821-9de27060c9e4",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        },
        {
            "id": "8339deb3-f1f6-4338-808c-ab7b7a23cd28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "compositeImage": {
                "id": "18fcb4cb-5c86-49d3-a63f-367dd7a89e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8339deb3-f1f6-4338-808c-ab7b7a23cd28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d04d2d-8a37-40a4-9289-b73eb46be990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8339deb3-f1f6-4338-808c-ab7b7a23cd28",
                    "LayerId": "dade514d-d70d-43bd-82cb-64ef32956924"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dade514d-d70d-43bd-82cb-64ef32956924",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd06f3cd-02be-4e40-b284-d752759bba09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}