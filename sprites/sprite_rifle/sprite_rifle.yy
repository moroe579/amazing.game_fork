{
    "id": "83aba440-7d73-4d2d-aef0-5ec8fe80a7d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa7ea7ed-2637-46a9-aefc-a7ef6139b284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83aba440-7d73-4d2d-aef0-5ec8fe80a7d0",
            "compositeImage": {
                "id": "dedd2ea3-99da-4544-813f-43cc7ebc9968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7ea7ed-2637-46a9-aefc-a7ef6139b284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b523519d-3610-4ba4-a006-87f7716ef378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7ea7ed-2637-46a9-aefc-a7ef6139b284",
                    "LayerId": "806027a8-fec4-4690-ae49-c503fe4a4629"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "806027a8-fec4-4690-ae49-c503fe4a4629",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83aba440-7d73-4d2d-aef0-5ec8fe80a7d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}