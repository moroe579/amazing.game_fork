{
    "id": "7facd491-3e6a-4023-90eb-6463b01e7c99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09fb986e-420d-4ff1-b328-1cee62fab65e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "54c6ac7a-6678-4777-a669-2cb270453b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09fb986e-420d-4ff1-b328-1cee62fab65e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88423ebd-4f6f-452f-ba51-87279c2cefaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09fb986e-420d-4ff1-b328-1cee62fab65e",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "3464e13a-4c0c-4261-8a40-d78b15849ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "03e1af42-e738-4005-9327-4376dadd6405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3464e13a-4c0c-4261-8a40-d78b15849ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ef2782-32bb-4176-ae92-05ffc6676e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3464e13a-4c0c-4261-8a40-d78b15849ccf",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "6d533785-806a-4b10-9d7d-e8acee5109a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "5e7f204a-c8a2-4d3c-9338-3d2a20a336c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d533785-806a-4b10-9d7d-e8acee5109a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35d3fe6-0730-4583-b413-ba78498239fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d533785-806a-4b10-9d7d-e8acee5109a0",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "32f6a97c-d00e-4662-abb3-cd890d49ebcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "bf31a64e-b8cf-4874-817d-86dff1623513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32f6a97c-d00e-4662-abb3-cd890d49ebcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda10d4d-2519-468e-a152-5c7cda33a704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32f6a97c-d00e-4662-abb3-cd890d49ebcd",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "e634b179-9ff0-4d1b-bbf1-6f053a2b3a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "33870a58-594b-49bb-b646-a96a0fbdde03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e634b179-9ff0-4d1b-bbf1-6f053a2b3a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb7faa4b-fc59-4821-8732-9876292aefe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e634b179-9ff0-4d1b-bbf1-6f053a2b3a40",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "9dbce982-24fb-4c91-928a-0021ed9a5847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "ee5c1314-217f-4357-904c-3cd99c397636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbce982-24fb-4c91-928a-0021ed9a5847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c92d89-11d0-4a60-8194-5036a39cb94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbce982-24fb-4c91-928a-0021ed9a5847",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "f24bce11-fe89-44b5-8b54-50fc82be55c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "1e5ec3bc-1910-4642-b504-47127dbaa74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f24bce11-fe89-44b5-8b54-50fc82be55c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fea17d-f9ca-4595-a628-cc5503250df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f24bce11-fe89-44b5-8b54-50fc82be55c3",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        },
        {
            "id": "35cec5fc-7ce4-4f38-a731-494aaadac234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "compositeImage": {
                "id": "a2fed456-e819-4ffd-9afe-b278bc465dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35cec5fc-7ce4-4f38-a731-494aaadac234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ccaefa8-5d20-4cb1-a2ff-caa3f22d2e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35cec5fc-7ce4-4f38-a731-494aaadac234",
                    "LayerId": "1423bdab-84a8-428c-b476-310132acdff7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "1423bdab-84a8-428c-b476-310132acdff7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7facd491-3e6a-4023-90eb-6463b01e7c99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 0
}