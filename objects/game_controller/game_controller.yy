{
    "id": "d4a82a53-32e1-4dc4-9d76-1d83806cbf8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game_controller",
    "eventList": [
        {
            "id": "c359e332-ef1b-4357-b1b9-6064b2211491",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4a82a53-32e1-4dc4-9d76-1d83806cbf8e"
        },
        {
            "id": "c7c8e5c2-67a5-4ea7-a5ff-7e1f5495fed9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4a82a53-32e1-4dc4-9d76-1d83806cbf8e"
        },
        {
            "id": "e14d193d-aeae-4b35-bf8a-ee55df738d48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d4a82a53-32e1-4dc4-9d76-1d83806cbf8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}