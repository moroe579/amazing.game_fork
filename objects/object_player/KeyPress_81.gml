/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0B4C7932
/// @DnDArgument : "var" "global.ammo_rifle"
if(global.ammo_rifle == 0)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 41402780
	/// @DnDParent : 0B4C7932
	/// @DnDArgument : "var" "gun_rifle"
	global.gun_rifle = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3D5D74BE
/// @DnDArgument : "var" "global.ammo_pistol"
if(global.ammo_pistol == 0)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 012F4280
	/// @DnDParent : 3D5D74BE
	/// @DnDArgument : "var" "gun_pistol"
	global.gun_pistol = 0;
}