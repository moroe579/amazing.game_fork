/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 21BA8068
/// @DnDArgument : "var" "gun"
gun = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 0C3B408D
/// @DnDArgument : "var" "gun_rifle"
global.gun_rifle = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 328CAA94
/// @DnDArgument : "var" "cooldown"
cooldown = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 035356DE
/// @DnDArgument : "var" "cooldown_rifle"
cooldown_rifle = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 520E2688
/// @DnDArgument : "var" "ammo_rifle"
global.ammo_rifle = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 170971CB
/// @DnDArgument : "var" "ammo_pistol"
global.ammo_pistol = 0;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 4932E6D2
/// @DnDArgument : "var" "gun_pistol"
global.gun_pistol = 0;