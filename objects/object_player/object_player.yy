{
    "id": "95f43734-4326-49f0-86d6-2c3d82374049",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "b6ec6eec-4e8f-4f0b-8114-da906d5aa93d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "fbe1b8fa-b672-42cd-a264-f6d99225e7a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "cb390ada-fff9-4016-8ea6-63fb3628e3b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "1acda155-38f7-490c-be4b-35736f6275e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "734cc1c3-b872-4dcd-b0da-4140eae6a8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "794b31a4-145b-4f32-9026-15c3549d69c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "9c8458b4-2a84-43e1-b011-1d70b1f3d9a4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "a3c44886-20de-45ed-b885-e3d920c5cd75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "678de4e6-a6ba-42a0-9b64-aeff0840e87d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "92ebc82b-ad3a-467d-9a09-10af4fdbeed1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "70541a25-a87f-4b47-86f3-9b0077980309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "fb507e33-ba2e-4c23-b0c6-f8c06e75395d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1aa3fd99-5e2b-4fe6-848e-3ecdbc1e3eed",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "730fa7d9-6451-415f-92c8-cf5358b22771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2c12f97b-9a5c-4f6d-a8fe-bd6a00bd2162",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "b0a6a7a4-ec7d-42f0-a2db-d9506b0421a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "d6318ae5-e3da-4119-963b-8be6cd8d25e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "ee533c45-16b6-4b66-89b2-d73aaf1bba25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "440322d5-088a-4faa-8a3c-3ed75c24c58b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "1e2b2dc8-3506-484a-86d3-28244333b59d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "63c5e65d-c31c-447e-9198-ea19c7fa8da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "ad082148-af49-4c4b-a2b5-dc8cda36ae43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "02f98e6c-06c3-4624-89af-153b1812abc5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "efa7bb0d-eccd-4a04-be9a-d602979721f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6ceef7a5-7e5e-4497-adf5-35c631223adf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "9b7b8563-cd34-4b98-a321-de4269b15495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 9,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "8cc5cc75-7beb-452f-a625-8cba6b16635b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4817853c-0612-4fed-b756-8f7853479333",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        },
        {
            "id": "1e54efd5-7fb2-4975-8c39-bf7361930c3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "97368cf6-25ca-484a-840b-c79303ffeced",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95f43734-4326-49f0-86d6-2c3d82374049"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44d488ff-9583-40d3-88e7-3149ed495921",
    "visible": true
}