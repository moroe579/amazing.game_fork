/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0BEE607B
/// @DnDArgument : "var" "global.gun_pistol"
/// @DnDArgument : "value" "1"
if(global.gun_pistol == 1)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
	/// @DnDVersion : 1
	/// @DnDHash : 551C151F
	/// @DnDParent : 0BEE607B
	/// @DnDArgument : "x" "mouse_x"
	/// @DnDArgument : "y" "mouse_y"
	direction = point_direction(x, y, mouse_x, mouse_y);

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Mouse_Pressed
	/// @DnDVersion : 1.1
	/// @DnDHash : 77695E7A
	/// @DnDParent : 0BEE607B
	var l77695E7A_0;
	l77695E7A_0 = mouse_check_button_pressed(mb_left);
	if (l77695E7A_0)
	{
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 25F00930
		/// @DnDParent : 77695E7A
		/// @DnDArgument : "var" "cooldown"
		/// @DnDArgument : "op" "3"
		if(cooldown <= 0)
		{
			/// @DnDAction : YoYo Games.Common.If_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 69795A7E
			/// @DnDParent : 25F00930
			/// @DnDArgument : "var" "global.ammo_pistol"
			/// @DnDArgument : "op" "2"
			if(global.ammo_pistol > 0)
			{
				/// @DnDAction : YoYo Games.Instances.Create_Instance
				/// @DnDVersion : 1
				/// @DnDHash : 0039F446
				/// @DnDParent : 69795A7E
				/// @DnDArgument : "xpos" "x"
				/// @DnDArgument : "ypos" "y"
				/// @DnDArgument : "objectid" "object_bullet"
				/// @DnDArgument : "layer" ""Instances_1""
				/// @DnDSaveInfo : "objectid" "1f80da6f-1923-48e6-a7fb-bb21d3ba2483"
				instance_create_layer(x, y, "Instances_1", object_bullet);
			
				/// @DnDAction : YoYo Games.Common.Variable
				/// @DnDVersion : 1
				/// @DnDHash : 5FFC6D91
				/// @DnDParent : 69795A7E
				/// @DnDArgument : "expr" "10"
				/// @DnDArgument : "var" "cooldown"
				cooldown = 10;
			
				/// @DnDAction : YoYo Games.Common.Set_Global
				/// @DnDVersion : 1
				/// @DnDHash : 3E89802B
				/// @DnDParent : 69795A7E
				/// @DnDArgument : "value" "global.ammo_pistol - 1"
				/// @DnDArgument : "var" "ammo_pistol"
				global.ammo_pistol = global.ammo_pistol - 1;
			}
		}
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3D20E3DD
/// @DnDArgument : "var" "global.gun_rifle"
/// @DnDArgument : "value" "1"
if(global.gun_rifle == 1)
{
	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Mouse_Down
	/// @DnDVersion : 1.1
	/// @DnDHash : 2DA06579
	/// @DnDParent : 3D20E3DD
	var l2DA06579_0;
	l2DA06579_0 = mouse_check_button(mb_left);
	if (l2DA06579_0)
	{
		/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
		/// @DnDVersion : 1
		/// @DnDHash : 5DB9EC10
		/// @DnDParent : 2DA06579
		/// @DnDArgument : "x" "mouse_x"
		/// @DnDArgument : "y" "mouse_y"
		direction = point_direction(x, y, mouse_x, mouse_y);
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 71E927A3
		/// @DnDParent : 2DA06579
		/// @DnDArgument : "var" "cooldown_rifle"
		/// @DnDArgument : "op" "3"
		if(cooldown_rifle <= 0)
		{
			/// @DnDAction : YoYo Games.Common.If_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 0367F070
			/// @DnDParent : 71E927A3
			/// @DnDArgument : "var" "global.ammo_rifle"
			/// @DnDArgument : "op" "2"
			if(global.ammo_rifle > 0)
			{
				/// @DnDAction : YoYo Games.Instances.Create_Instance
				/// @DnDVersion : 1
				/// @DnDHash : 282F6162
				/// @DnDParent : 0367F070
				/// @DnDArgument : "xpos" "x"
				/// @DnDArgument : "ypos" "y"
				/// @DnDArgument : "objectid" "object_bullet"
				/// @DnDArgument : "layer" ""Instances_1""
				/// @DnDSaveInfo : "objectid" "1f80da6f-1923-48e6-a7fb-bb21d3ba2483"
				instance_create_layer(x, y, "Instances_1", object_bullet);
			
				/// @DnDAction : YoYo Games.Common.Variable
				/// @DnDVersion : 1
				/// @DnDHash : 4A5BC008
				/// @DnDParent : 0367F070
				/// @DnDArgument : "expr" "4"
				/// @DnDArgument : "var" "cooldown_rifle"
				cooldown_rifle = 4;
			
				/// @DnDAction : YoYo Games.Common.Variable
				/// @DnDVersion : 1
				/// @DnDHash : 51990651
				/// @DnDParent : 0367F070
				/// @DnDArgument : "expr" "global.ammo_rifle-1"
				/// @DnDArgument : "var" "global.ammo_rifle"
				global.ammo_rifle = global.ammo_rifle-1;
			}
		}
	}
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 7E610F0C
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "cooldown"
cooldown += -1;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 51E45734
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "cooldown_rifle"
cooldown_rifle += -1;