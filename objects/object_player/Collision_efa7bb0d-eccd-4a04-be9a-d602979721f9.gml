/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 13ED5947
/// @DnDArgument : "var" "global.gun_rifle"
/// @DnDArgument : "value" "1"
if(global.gun_rifle == 1)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 437678F9
	/// @DnDParent : 13ED5947
	/// @DnDArgument : "expr" "global.ammo_rifle + 30"
	/// @DnDArgument : "var" "global.ammo_rifle"
	global.ammo_rifle = global.ammo_rifle + 30;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 027CB575
/// @DnDArgument : "var" "global.gun_pistol"
/// @DnDArgument : "value" "1"
if(global.gun_pistol == 1)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1C8FC8D1
	/// @DnDParent : 027CB575
	/// @DnDArgument : "value" "global.ammo_pistol + 15"
	/// @DnDArgument : "var" "ammo_pistol"
	global.ammo_pistol = global.ammo_pistol + 15;
}