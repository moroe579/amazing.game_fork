{
    "id": "678de4e6-a6ba-42a0-9b64-aeff0840e87d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_door",
    "eventList": [
        {
            "id": "a7fa3a61-a737-41df-855c-de6b2a250751",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f43734-4326-49f0-86d6-2c3d82374049",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "678de4e6-a6ba-42a0-9b64-aeff0840e87d"
        },
        {
            "id": "fafd0534-b15a-4a93-93b0-1b5ceefc552a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "678de4e6-a6ba-42a0-9b64-aeff0840e87d"
        },
        {
            "id": "87352fb9-02bc-42e5-a53c-ab09797e20af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "678de4e6-a6ba-42a0-9b64-aeff0840e87d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
    "visible": true
}