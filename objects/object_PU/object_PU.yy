{
    "id": "97368cf6-25ca-484a-840b-c79303ffeced",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_PU",
    "eventList": [
        {
            "id": "64a17068-73d5-4620-972a-ac0e38e574e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97368cf6-25ca-484a-840b-c79303ffeced"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1167cf7e-709e-4fc7-8dfe-d295191c92f0",
    "visible": true
}