/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 782A291B
instance_destroy();

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 7CA25C57
/// @DnDArgument : "xpos" "x"
/// @DnDArgument : "ypos" "y"
/// @DnDArgument : "objectid" "object_explosion"
/// @DnDArgument : "layer" ""Instances_1""
/// @DnDSaveInfo : "objectid" "d80f1a23-77bd-4c1f-8066-6ab847916022"
instance_create_layer(x, y, "Instances_1", object_explosion);