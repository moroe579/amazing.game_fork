{
    "id": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "bf374750-860a-47c5-ba7e-564d01fb047d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483"
        },
        {
            "id": "aa3639b4-df09-4d86-ad22-863e6eb4e3fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "9c8458b4-2a84-43e1-b011-1d70b1f3d9a4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483"
        },
        {
            "id": "c5219914-6fb9-48fc-8246-65e7d17c3d54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fd0ac46c-9856-4c4b-9423-b9006b73af71",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd50b019-0133-4971-975e-13c9b58dc274",
    "visible": true
}