{
    "id": "fd0ac46c-9856-4c4b-9423-b9006b73af71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_skeleton",
    "eventList": [
        {
            "id": "8ca6ee14-71ad-46dd-b5c1-ecd2bd38ba64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd0ac46c-9856-4c4b-9423-b9006b73af71"
        },
        {
            "id": "46ce199b-1a26-4702-86ea-7ae41d6e8ad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fd0ac46c-9856-4c4b-9423-b9006b73af71"
        },
        {
            "id": "5b8a874b-b51f-454f-b0d6-45a607bd99a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd0ac46c-9856-4c4b-9423-b9006b73af71"
        },
        {
            "id": "8bfa50db-c2e0-4d85-b4cd-867ca6ffe42c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4817853c-0612-4fed-b756-8f7853479333",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fd0ac46c-9856-4c4b-9423-b9006b73af71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1aa3fd99-5e2b-4fe6-848e-3ecdbc1e3eed",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "981b712a-ea8c-46b3-8fb4-fa69b3b5949b",
    "visible": true
}