/// @DnDAction : YoYo Games.Rooms.If_First_Room
/// @DnDVersion : 1
/// @DnDHash : 7FD43162
if(room == room_first)
{
	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 0647A4E1
	/// @DnDParent : 7FD43162
	/// @DnDArgument : "path" "path_1A"
	/// @DnDArgument : "speed" "3"
	/// @DnDArgument : "atend" "path_action_reverse"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "09c484c8-8bc0-4543-a431-54bbdfaf3842"
	path_start(path_1A, 3, path_action_reverse, true);

	/// @DnDAction : YoYo Games.Paths.Path_Position
	/// @DnDVersion : 1
	/// @DnDHash : 7F75F062
	/// @DnDParent : 7FD43162
	/// @DnDArgument : "position" "random(1)"
	path_position = random(1);
}

/// @DnDAction : YoYo Games.Rooms.If_First_Room
/// @DnDVersion : 1
/// @DnDHash : 3DA7D49A
/// @DnDArgument : "not" "1"
if(room != room_first)
{
	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 1C339675
	/// @DnDParent : 3DA7D49A
	/// @DnDArgument : "path" "path_1B"
	/// @DnDArgument : "speed" "3"
	/// @DnDArgument : "atend" "path_action_reverse"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "a1994eec-7a53-4415-ab0b-18e24c2d099c"
	path_start(path_1B, 3, path_action_reverse, true);

	/// @DnDAction : YoYo Games.Paths.Path_Position
	/// @DnDVersion : 1
	/// @DnDHash : 532732E2
	/// @DnDParent : 3DA7D49A
	/// @DnDArgument : "position" "random(1)"
	path_position = random(1);
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 455E0B43
/// @DnDArgument : "expr" "3"
/// @DnDArgument : "var" "hp"
hp = 3;