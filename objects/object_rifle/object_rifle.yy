{
    "id": "02f98e6c-06c3-4624-89af-153b1812abc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_rifle",
    "eventList": [
        {
            "id": "0846f046-4d8c-4259-b8ca-504bd372a080",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f43734-4326-49f0-86d6-2c3d82374049",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "02f98e6c-06c3-4624-89af-153b1812abc5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83aba440-7d73-4d2d-aef0-5ec8fe80a7d0",
    "visible": true
}