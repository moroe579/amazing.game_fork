{
    "id": "d80f1a23-77bd-4c1f-8066-6ab847916022",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_explosion",
    "eventList": [
        {
            "id": "89da2257-a68b-49e4-ad93-d22404222ec3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d80f1a23-77bd-4c1f-8066-6ab847916022"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3629074-ad35-4077-bbcb-01738a4eea85",
    "visible": true
}