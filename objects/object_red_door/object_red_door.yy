{
    "id": "4817853c-0612-4fed-b756-8f7853479333",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_door",
    "eventList": [
        {
            "id": "81453dc2-aeb8-4af5-9cf7-d0de5c7f9a83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4817853c-0612-4fed-b756-8f7853479333"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "253956ca-a79e-4090-9ab7-5db0632ee9e0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "89071090-1dce-415e-ba2f-f4c223208ed0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "71226b92-ff81-4e09-aaf7-eed8ab9eb8fc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "234f87a2-fe4c-4826-beb6-ead0567b0ab8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "b9ac9e63-dccb-4288-86d3-f9d13633a625",
    "visible": true
}