{
    "id": "b8aff73d-7493-48ff-8b45-02fb51036410",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_switch",
    "eventList": [
        {
            "id": "57ea96e9-13e9-454d-8c81-d9b1d0e01caf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8aff73d-7493-48ff-8b45-02fb51036410"
        },
        {
            "id": "67e2339f-4f4d-4285-ad15-2eb93d24b5e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b8aff73d-7493-48ff-8b45-02fb51036410"
        },
        {
            "id": "eea5f486-f659-4f45-8bf2-2c184d4fe4a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b8aff73d-7493-48ff-8b45-02fb51036410"
        },
        {
            "id": "970b420e-5b76-4ed6-a60e-38c1abebcdc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8aff73d-7493-48ff-8b45-02fb51036410"
        },
        {
            "id": "d942676d-fd95-47e1-8be4-6083593bd34e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f43734-4326-49f0-86d6-2c3d82374049",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b8aff73d-7493-48ff-8b45-02fb51036410"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07493c15-420b-4181-a389-378f1cbcf683",
    "visible": true
}