{
    "id": "6ceef7a5-7e5e-4497-adf5-35c631223adf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ammo",
    "eventList": [
        {
            "id": "40a649f8-17e2-4aad-92af-6ea555293fb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f43734-4326-49f0-86d6-2c3d82374049",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ceef7a5-7e5e-4497-adf5-35c631223adf"
        },
        {
            "id": "9fa0c870-64ed-4c76-9005-9b7d07608171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ceef7a5-7e5e-4497-adf5-35c631223adf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "96a124b7-2229-4bd1-b5da-2b94297a2fe1",
    "visible": true
}