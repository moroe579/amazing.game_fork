/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04AA6BA6
/// @DnDArgument : "var" "global.gun_rifle"
/// @DnDArgument : "value" "1"
if(global.gun_rifle == 1)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 4C3FAA83
	/// @DnDParent : 04AA6BA6
	instance_destroy();
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1DA98986
/// @DnDArgument : "var" "global.gun_pistol"
/// @DnDArgument : "value" "1"
if(global.gun_pistol == 1)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 05BE968C
	/// @DnDParent : 1DA98986
	instance_destroy();
}