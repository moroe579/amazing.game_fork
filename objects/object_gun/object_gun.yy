{
    "id": "2c12f97b-9a5c-4f6d-a8fe-bd6a00bd2162",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_gun",
    "eventList": [
        {
            "id": "93b67793-a28a-4b31-bc31-a4409895ec6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f43734-4326-49f0-86d6-2c3d82374049",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2c12f97b-9a5c-4f6d-a8fe-bd6a00bd2162"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "318c1cba-205f-4650-ad2d-00d115f86af8",
    "visible": true
}