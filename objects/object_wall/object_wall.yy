{
    "id": "9c8458b4-2a84-43e1-b011-1d70b1f3d9a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "d1cd20c1-8c82-47c9-bfca-57d121e8dfdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c8458b4-2a84-43e1-b011-1d70b1f3d9a4"
        },
        {
            "id": "4f2fcff3-42dc-4bac-97e3-817db30cf7fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1f80da6f-1923-48e6-a7fb-bb21d3ba2483",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9c8458b4-2a84-43e1-b011-1d70b1f3d9a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6",
    "visible": true
}