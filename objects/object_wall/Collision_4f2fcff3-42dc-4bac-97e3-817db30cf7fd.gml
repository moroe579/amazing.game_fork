/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 36A7665E
/// @DnDArgument : "expr" "wall_health-1"
/// @DnDArgument : "var" "wall_health"
wall_health = wall_health-1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4A56A6C3
/// @DnDArgument : "var" "wall_health"
/// @DnDArgument : "op" "3"
if(wall_health <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 28890040
	/// @DnDParent : 4A56A6C3
	instance_destroy();
}