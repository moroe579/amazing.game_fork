/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 4B855193
/// @DnDArgument : "imageind" "irandom(2)"
/// @DnDArgument : "spriteind" "sprite_wall_blocks"
/// @DnDSaveInfo : "spriteind" "8a9f7f4d-01aa-4d85-97a6-c8ea42b83fb6"
sprite_index = sprite_wall_blocks;
image_index = irandom(2);

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 3692753E
/// @DnDArgument : "speed" "0 "
image_speed = 0 ;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 652F03FA
/// @DnDArgument : "expr" "5"
/// @DnDArgument : "var" "wall_health"
wall_health = 5;